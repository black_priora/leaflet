// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"map.js":[function(require,module,exports) {
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createMap;
function createMap(_ref) {
  var container = _ref.container,
      _ref$coordinates = _ref.coordinates,
      coordinates = _ref$coordinates === undefined ? [39.74739, -105] : _ref$coordinates,
      _ref$zoom = _ref.zoom,
      zoom = _ref$zoom === undefined ? 15 : _ref$zoom;

  var map = L.map(container).setView(coordinates, zoom);

  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18
  }).addTo(map);

  return map;
}
},{}],"geoJSON.js":[function(require,module,exports) {
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var freeBus = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    geometry: {
      type: 'LineString',
      coordinates: [[-105.00341892242432, 39.75383843460583], [-105.0008225440979, 39.751891803969535]]
    },
    properties: {
      popupContent: 'This is a free bus line that will take you across downtown.',
      underConstruction: false
    },
    id: 1
  }, {
    type: 'Feature',
    geometry: {
      type: 'LineString',
      coordinates: [[-105.0008225440979, 39.751891803969535], [-104.99820470809937, 39.74979664004068]]
    },
    properties: {
      popupContent: 'This is a free bus line that will take you across downtown.',
      underConstruction: true
    },
    id: 2
  }, {
    type: 'Feature',
    geometry: {
      type: 'LineString',
      coordinates: [[-104.99820470809937, 39.74979664004068], [-104.98689651489258, 39.741052354709055]]
    },
    properties: {
      popupContent: 'This is a free bus line that will take you across downtown.',
      underConstruction: false
    },
    id: 3
  }]
};

var lightRailStop = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    properties: {
      popupContent: '18th & California Light Rail Stop'
    },
    geometry: {
      type: 'Point',
      coordinates: [-104.98999178409576, 39.74683938093904]
    }
  }, {
    type: 'Feature',
    properties: {
      popupContent: '20th & Welton Light Rail Stop'
    },
    geometry: {
      type: 'Point',
      coordinates: [-104.98689115047453, 39.747924136466565]
    }
  }]
};

var bicycleRental = {
  type: 'FeatureCollection',
  features: [{
    geometry: {
      type: 'Point',
      coordinates: [-104.9998241, 39.7471494]
    },
    type: 'Feature',
    properties: {
      popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
    },
    id: 51
  }, {
    geometry: {
      type: 'Point',
      coordinates: [-104.9983545, 39.7502833]
    },
    type: 'Feature',
    properties: {
      popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
    },
    id: 52
  }, {
    geometry: {
      type: 'Point',
      coordinates: [-104.9963919, 39.7444271]
    },
    type: 'Feature',
    properties: {
      popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
    },
    id: 54
  }, {
    geometry: {
      type: 'Point',
      coordinates: [-104.9960754, 39.7498956]
    },
    type: 'Feature',
    properties: {
      popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
    },
    id: 55
  }, {
    geometry: {
      type: 'Point',
      coordinates: [-104.9933717, 39.7477264]
    },
    type: 'Feature',
    properties: {
      popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
    },
    id: 57
  }, {
    geometry: {
      type: 'Point',
      coordinates: [-104.9913392, 39.7432392]
    },
    type: 'Feature',
    properties: {
      popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
    },
    id: 58
  }, {
    geometry: {
      type: 'Point',
      coordinates: [-104.9788452, 39.6933755]
    },
    type: 'Feature',
    properties: {
      popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
    },
    id: 74
  }]
};

var campus = {
  type: 'Feature',
  properties: {
    popupContent: 'This is the Auraria West Campus',
    style: {
      weight: 2,
      color: '#999',
      opacity: 1,
      fillColor: '#B0DE5C',
      fillOpacity: 0.8
    }
  },
  geometry: {
    type: 'MultiPolygon',
    coordinates: [[[[-105.00432014465332, 39.74732195489861], [-105.00715255737305, 39.7462000683517], [-105.00921249389647, 39.74468219277038], [-105.01067161560059, 39.74362625960105], [-105.01195907592773, 39.74290029616054], [-105.00989913940431, 39.74078835902781], [-105.00758171081543, 39.74059036160317], [-105.00346183776855, 39.74059036160317], [-105.00097274780272, 39.74059036160317], [-105.00062942504881, 39.74072235994946], [-105.00020027160645, 39.74191033368865], [-105.00071525573731, 39.74276830198601], [-105.00097274780272, 39.74369225589818], [-105.00097274780272, 39.74461619742136], [-105.00123023986816, 39.74534214278395], [-105.00183105468751, 39.74613407445653], [-105.00432014465332, 39.74732195489861]], [[-105.00361204147337, 39.74354376414072], [-105.00301122665405, 39.74278480127163], [-105.00221729278564, 39.74316428375108], [-105.00283956527711, 39.74390674342741], [-105.00361204147337, 39.74354376414072]]], [[[-105.00942707061768, 39.73989736613708], [-105.00942707061768, 39.73910536278566], [-105.00685214996338, 39.73923736397631], [-105.00384807586671, 39.73910536278566], [-105.00174522399902, 39.73903936209552], [-105.00041484832764, 39.73910536278566], [-105.00041484832764, 39.73979836621592], [-105.00535011291504, 39.73986436617916], [-105.00942707061768, 39.73989736613708]]]]
  }
};

var coorsField = {
  type: 'Feature',
  properties: {
    popupContent: 'Coors Field'
  },
  geometry: {
    type: 'Point',
    coordinates: [-104.99404191970824, 39.756213909328125]
  }
};

exports.freeBus = freeBus;
exports.bicycleRental = bicycleRental;
},{}],"app.js":[function(require,module,exports) {
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _map = require('./map');

var _map2 = _interopRequireDefault(_map);

var _geoJSON = require('./geoJSON');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BaseFeatureLayer = function (_L$GeoJSON) {
  _inherits(BaseFeatureLayer, _L$GeoJSON);

  function BaseFeatureLayer() {
    _classCallCheck(this, BaseFeatureLayer);

    return _possibleConstructorReturn(this, (BaseFeatureLayer.__proto__ || Object.getPrototypeOf(BaseFeatureLayer)).apply(this, arguments));
  }

  _createClass(BaseFeatureLayer, [{
    key: 'initialize',
    value: function initialize() {
      this.initUserInteractionEvents();
      L.GeoJSON.prototype.initialize.apply(this, arguments);
    }
  }, {
    key: 'initUserInteractionEvents',
    value: function initUserInteractionEvents() {
      var that = this;

      Object.assign(this.options, {
        onEachFeature: function onEachFeature(feature, layer) {
          layer.on({
            click: function click() {
              that.fire('featureClick', { feature: feature, layer: layer });
            }
          });
        }
      });
    }
  }, {
    key: 'styles',
    value: function styles() {
      return {
        // TODO
        // add feature styles
      };
    }
  }, {
    key: 'watchMapZoom',
    value: function watchMapZoom() {
      // TODO
      // this.map.on('zoom')
    }
  }, {
    key: 'onMapZoom',
    value: function onMapZoom(zoom) {
      // TODO
      // show/hide layer
    }
  }]);

  return BaseFeatureLayer;
}(L.GeoJSON);

var Entrances = new BaseFeatureLayer(_geoJSON.bicycleRental);

Entrances.on('featureClick', function (feature, layer) {
  console.log(feature, layer);
});

var map = (0, _map2.default)({ container: 'map' });

Entrances.addTo(map);
},{"./map":"map.js","./geoJSON":"geoJSON.js"}],"../../../.config/yarn/global/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';

var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };

  module.bundle.hotData = null;
}

module.bundle.Module = Module;

var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = '' || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + '55496' + '/');
  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();

      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });

      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();
      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');

      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);

      removeErrorOverlay();

      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);
  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID;

  // html encode message and stack trace
  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;

  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';

  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];
      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};
  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);

  cached = bundle.cache[id];
  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../../.config/yarn/global/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","app.js"], null)
//# sourceMappingURL=/app.0f52fd26.map