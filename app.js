import createMap from './map'
import { bicycleRental } from './geoJSON'

class BaseFeatureLayer extends L.GeoJSON {
  initialize() {
    this.initUserInteractionEvents()
    L.GeoJSON.prototype.initialize.apply(this, arguments)
  }

  initUserInteractionEvents() {
    let that = this

    Object.assign(this.options, {
      onEachFeature(feature, layer) {
        layer.on({
          click: () => {
            that.fire('featureClick', { feature, layer })
          }
        })
      }
    })
  }

  styles() {
    return {
      // TODO
      // add feature styles
    }
  }

  watchMapZoom() {
    // TODO
    // this.map.on('zoom')
  }

  onMapZoom(zoom) {
    // TODO
    // show/hide layer
  }
}

let Entrances = new BaseFeatureLayer(bicycleRental)

Entrances.on('featureClick', (feature, layer) => {
  console.log(feature, layer)
})

let map = createMap({ container: 'map' })

Entrances.addTo(map)
